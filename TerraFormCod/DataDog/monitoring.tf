# Terraform 0.13+ uses the Terraform Registry:
terraform {
  required_providers {
    datadog = {
      source = "DataDog/datadog"
    }
  }
}

# Configure the Datadog provider
provider "datadog" {
  api_key = var.DD_API_KEY
  app_key = var.DD_APP_KEY
  api_url = var.api_url
}


resource "datadog_dashboard" "ordered_dashboard" {
  title        = "Ordered Layout Dashboard"
  description  = "Created using the Datadog provider in Terraform"
  layout_type  = "ordered"
  is_read_only = false

  widget {
    alert_graph_definition {
      alert_id  = "895605"
      viz_type  = "timeseries"
      title     = "Widget Title"
      live_span = "1h"
    }
  }
}